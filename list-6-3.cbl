       IDENTIFICATION DIVISION. 
       PROGRAM-ID. LISTING-6-3.
       AUTHOR. NARACHAI.
      
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  REP-COUNT         PIC 9(4).
       01  PRN-REP-CONT      PIC Z,ZZ9.
       01  NUMBER-OF-TIMES   PIC 9(4) VALUE 1000.

       PROCEDURE DIVISION.
       000-BEGIN.
           PERFORM VARYING REP-COUNT FROM 0 BY 50
              UNTIL REP-COUNT = NUMBER-OF-TIMES 
              MOVE REP-COUNT TO PRN-REP-CONT 
              DISPLAY "counting" PRN-REP-CONT 
           END-PERFORM
           MOVE REP-COUNT TO PRN-REP-CONT 
           DISPLAY "If I have told you once."
           DISPLAY "I've too you " PRN-REP-CONT " times"
           GOBACK 
       .
