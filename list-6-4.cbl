       IDENTIFICATION DIVISION. 
       PROGRAM-ID. LISTING-6-4.
       AUTHOR. NARACHAI.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  COUNTERS.
           05 HUDREDS-COUNT  PIC 99 VALUE ZEROS.
           05 TENS-COUNT     PIC 99 VALUE ZEROS.
           05 UNITS-COUNT    PIC 99 VALUE ZEROS.
       01  ODOMETE.
           05 PRN-HUDREDS    PIC 9.
           05 FILLER         PIC X VALUE "-".
           05 PRN-TENS       PIC 9.
           05 FILLER         PIC X VALUE "-".
           05 PRN-UNITS      PIC 9.

       PROCEDURE DIVISION.
       000-BEGIN.
           DISPLAY "Using an out-of-line Perform"
           PERFORM 001-COUNT-MLEAGE THRU 001-EXIT 
              VARYING HUDREDS-COUNT FROM 0 BY 1 UNTIL HUDREDS-COUNT > 9
              AFTER TENS-COUNT FROM 0 BY 1 UNTIL TENS-COUNT > 9
              AFTER UNITS-COUNT FROM 0 BY 1 UNTIL UNITS-COUNT > 9
           GOBACK 
       .

       001-COUNT-MLEAGE.
           MOVE HUDREDS-COUNT TO PRN-HUDREDS 
           MOVE TENS-COUNT TO PRN-TENS 
           MOVE UNITS-COUNT TO PRN-UNITS 
           DISPLAY "Out - " ODOMETE 
       .
       001-EXIT.
           EXIT
       .
